using DomainSearch.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace DomainSearch.DataModel
{
    class DataSource
    {
		//调用了万网的api接口
        public string _url = "http://panda.www.net.cn/cgi-bin/check_ac2.cgi";
        public string _domain = "";
        public int _quertType = 0;
        public List<string> suff = null;

		//查询选中的域
        public string buildQuery()
        {
            string domainStr = "";
            foreach (string val in suff)
            {
                domainStr += _domain + val + ",";
            }

            return domainStr;
        }

		//执行查询
        public async Task<string> doQuery()
        { 
            CommonTools tools = new CommonTools();
            string url = string.Empty;
            string text = string.Empty;
			//这里分英文查询和中文查询的api接口不一样
            if (_quertType == 1)
            {
                url = _url;
            }
            else
            {
                _url = "http://pandavip.www.net.cn/check/check_ac1.cgi";
            }
            url = _url + "?domain=" + buildQuery();
			//获取查询结果
            text = await tools.GetDataAsync(url, "gb2312");

            return text.Trim(new char[]{'"','(',')',';'});
        }

        //初始化ListView数据源
        public async Task<List<Grid>> initListItem()
        {
            string ret = await doQuery();
            if (ret == null || ret.Equals(""))
            {
                MessageDialog dlg = new MessageDialog("不能获取到网络数据");
                await dlg.ShowAsync();
                return null;
            }
            List<Grid> listGrid = new List<Grid>();
            int i = 1;
            string[] pieces = ret.Split('#');
			//用调用组件对象的方式,初始化一个ListView的item 
            foreach (string item in pieces)
            {
                string[] vals = item.Split('|');
				//初始化一个Grid
                Grid grid = new Grid();
                grid.Width = 550;
                grid.Height = 50;
                grid.Tag = item;
                if (i % 2 == 1)
                {
                    grid.Background = new SolidColorBrush(Color.FromArgb(122, 231, 231, 231));
                }
				//初始化一个TextBlock
                TextBlock tb = new TextBlock();
                tb.Margin = new Thickness(5, 5, 0, 0);
                tb.Width = 400;
                tb.Height = 50;
                tb.FontSize = 30;
                tb.Text = vals[1];
                tb.HorizontalAlignment = HorizontalAlignment.Left;
                tb.TextWrapping = TextWrapping.Wrap;
                tb.VerticalAlignment = VerticalAlignment.Center;

                string info = string.Empty;
				//显示不同颜色的状态
                Color color = Color.FromArgb(122, 255, 0, 0);
                if (vals[2] == "210")
                {
                    info = "可以注册";
                    color = Color.FromArgb(122, 0, 255, 0);
                }
                else if (vals[2] == "211")
                {
                    info = "已被注册";
                }
                else if (vals[2] == "212")
                {
                    info = "禁止注册";
                }
                else
                {
                    info = "网络异常";
                    color = Color.FromArgb(122, 0, 0, 255);
                }
                TextBlock tb1 = new TextBlock();
                tb1.Margin = new Thickness(400, 5, 0, 0);
                tb1.Width = 150;
                tb1.Height = 50;
                tb1.FontSize = 30;
                tb1.Text = info;
                tb1.Foreground = new SolidColorBrush(color);
                tb1.HorizontalAlignment = HorizontalAlignment.Left;
                tb1.TextWrapping = TextWrapping.Wrap;
                tb1.VerticalAlignment = VerticalAlignment.Center;

                grid.Children.Add(tb);
                grid.Children.Add(tb1);
                listGrid.Add(grid);

                i++;
            }
            return listGrid;
        }
    }
}
