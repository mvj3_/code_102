using DomainSearch.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Connectivity;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// “空白页”项模板在 http://go.microsoft.com/fwlink/?LinkId=234238 上有介绍

namespace DomainSearch
{
    /// <summary>
    /// 可用于自身或导航至 Frame 内部的空白页。
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //隐私声明设置---start
        Rect _windowBounds;
        double _settingsWidth = 346;
        Popup _settingsPopup;
        //隐私声明设置---end

        private DataSource data = new DataSource();

        public MainPage()
        {
            //隐私声明设置---start
            _windowBounds = Window.Current.Bounds;
            Window.Current.SizeChanged += OnWindowSizeChanged;
            SettingsPane.GetForCurrentView().CommandsRequested += BlankPage_CommandsRequested;
            //隐私声明设置---end

            this.InitializeComponent();
        }

        /// <summary>
        /// 在此页将要在 Frame 中显示时进行调用。
        /// </summary>
        /// <param name="e">描述如何访问此页的事件数据。Parameter
        /// 属性通常用于配置页。</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        //隐私声明设置---start
        void OnWindowSizeChanged(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {
            _windowBounds = Window.Current.Bounds;
        }

        void BlankPage_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            SettingsCommand cmd = new SettingsCommand("privacy", "隐私申明", (x) =>
            {
                _settingsPopup = new Popup();
                _settingsPopup.Closed += OnPopupClosed;
                Window.Current.Activated += OnWindowActivated;
                _settingsPopup.IsLightDismissEnabled = true;
                _settingsPopup.Width = _settingsWidth;
                _settingsPopup.Height = _windowBounds.Height;

                PrivacySettingPane mypane = new PrivacySettingPane();
                mypane.Width = _settingsWidth;
                mypane.Height = _windowBounds.Height;

                _settingsPopup.Child = mypane;
                _settingsPopup.SetValue(Canvas.LeftProperty, _windowBounds.Width - _settingsWidth);
                _settingsPopup.SetValue(Canvas.TopProperty, 0);
                _settingsPopup.IsOpen = true;
            });

            args.Request.ApplicationCommands.Add(cmd);

        }

        private void OnWindowActivated(object sender, Windows.UI.Core.WindowActivatedEventArgs e)
        {
            if (e.WindowActivationState == Windows.UI.Core.CoreWindowActivationState.Deactivated)
            {
                _settingsPopup.IsOpen = false;
            }
        }

        void OnPopupClosed(object sender, object e)
        {
            Window.Current.Activated -= OnWindowActivated;
        }
        //隐私声明设置---end

        //判断当前网络是否可用
        private bool IsConnectedToInternet()
        {
            bool connected = false;
            ConnectionProfile cp = NetworkInformation.GetInternetConnectionProfile();
            if (cp != null)
            {
                NetworkConnectivityLevel cl = cp.GetNetworkConnectivityLevel();
                connected = cl == NetworkConnectivityLevel.InternetAccess;
            }
            return connected;
        }

        private void RadioButton_Click_1(object sender, RoutedEventArgs e)
        {
            RadioButton checkBox = ((RadioButton)sender);
            if (checkBox.Content.Equals("英文域名") && checkBox.IsChecked == true)
            {
                foreach (CheckBox check in enDomain.Children)
                {
                    check.IsEnabled = true;
                    check.IsChecked = false;
                }
                foreach (CheckBox check in cnDomian.Children)
                {
                    check.IsEnabled = false;
                    check.IsChecked = false;
                }
                data._quertType = 1;
            }
            if (checkBox.Content.Equals("中文域名") && checkBox.IsChecked == true)
            {
                foreach (CheckBox check in enDomain.Children)
                {
                    check.IsChecked = false;
                    if (check.Content.Equals(".com") || check.Content.Equals(".cn") || check.Content.Equals(".net"))
                        continue;
                    check.IsEnabled = false;
                }
                foreach (CheckBox check in cnDomian.Children)
                {
                    check.IsEnabled = true;
                    check.IsChecked = false;
                }
                data._quertType = 2;
            }
            data._domain = "";
            data.suff = null;
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string domain = domainTB.Text;
            if (domain.Equals(""))
            {
                MessageDialog dialog = new MessageDialog("请填写域名");
                await dialog.ShowAsync();
                return;
            }
            if (data.suff == null)
            {
                MessageDialog dialog = new MessageDialog("请选择域名后缀");
                await dialog.ShowAsync();
                return;
            }
            loading.IsActive = true;
            data._domain = domain;
            resultListView.ItemsSource = null;
            List<Grid> list = await data.initListItem();
            resultListView.ItemsSource = list;
            loading.IsActive = false;
        }

        private void CheckBox_Click_1(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = ((CheckBox)sender);
            if (data.suff == null)
            {
                data.suff = new List<string>();
            }
            if (checkBox.IsChecked == true)
            {
                if (data.suff != null && data.suff.Contains(checkBox.Content.ToString()))
                {
                    return;
                }
                data.suff.Add(checkBox.Content.ToString());
            }
            else
            {
                data.suff.Remove(checkBox.Content.ToString());
            }

            List<string> li = data.suff;
        }

    }
}
